import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _37cc9c3c = () => interopDefault(import('../pages/login.vue' /* webpackChunkName: "pages/login" */))
const _5b634775 = () => interopDefault(import('../pages/my-champ.vue' /* webpackChunkName: "pages/my-champ" */))
const _376f93ce = () => interopDefault(import('../pages/customer/form.vue' /* webpackChunkName: "pages/customer/form" */))
const _6b68baa8 = () => interopDefault(import('../pages/customer/list.vue' /* webpackChunkName: "pages/customer/list" */))
const _24a0d51c = () => interopDefault(import('../pages/customer/list-filter-paging.vue' /* webpackChunkName: "pages/customer/list-filter-paging" */))
const _7ac0d720 = () => interopDefault(import('../pages/my-menu/logout.vue' /* webpackChunkName: "pages/my-menu/logout" */))
const _0357e486 = () => interopDefault(import('../pages/customer/detail/_id.vue' /* webpackChunkName: "pages/customer/detail/_id" */))
const _b16bf482 = () => interopDefault(import('../pages/customer/edit/_id.vue' /* webpackChunkName: "pages/customer/edit/_id" */))
const _1a720ecb = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _37cc9c3c,
    name: "login"
  }, {
    path: "/my-champ",
    component: _5b634775,
    name: "my-champ"
  }, {
    path: "/customer/form",
    component: _376f93ce,
    name: "customer-form"
  }, {
    path: "/customer/list",
    component: _6b68baa8,
    name: "customer-list"
  }, {
    path: "/customer/list-filter-paging",
    component: _24a0d51c,
    name: "customer-list-filter-paging"
  }, {
    path: "/my-menu/logout",
    component: _7ac0d720,
    name: "my-menu-logout"
  }, {
    path: "/customer/detail/:id?",
    component: _0357e486,
    name: "customer-detail-id"
  }, {
    path: "/customer/edit/:id?",
    component: _b16bf482,
    name: "customer-edit-id"
  }, {
    path: "/",
    component: _1a720ecb,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
